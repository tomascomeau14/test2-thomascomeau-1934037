package Question2;

/**
 * An employee of type hourly that implements the employee interface
 * @author tomas
 *
 */
public class HourlyEmployee implements Employee {
	/**
	 * Represents the hours they work in a week
	 */
	private int hours;
	/**
	 * Represents the amount they make per hour
	 */
	private int wage;
	
	/**
	 * @return the yearly pay of the employee
	 */
	public int getYearlyPay() {
		int yearPay = (hours * wage) * 52;
		return yearPay;
	}
	
	/**
	 * Makes an employee of type hourly
	 * @param hours
	 * @param wage
	 */
	public HourlyEmployee(int hours,int wage) {
		this.hours = hours;
		this.wage = wage;
	}
}

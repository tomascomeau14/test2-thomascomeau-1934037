package Question2;

/**
 * To manage the payroll of all employees
 * @author tomas
 *
 */
public class PayrollManagement {
	
	/**
	 * 
	 * @param employee
	 * @return The total amount of expense you will have to pay all employees this year
	 */
	public static int getTotalExpenses(Employee[] employee) {
		int total = 0;
		for(int i = 0;i<employee.length;i++) {
			total = total + employee[i].getYearlyPay();
		}
		return total;
	}
	
	/**
	 * The driver method for PayrollManagement
	 * @param args
	 */
	public static void main(String[]args) {
		Employee[] employee = new Employee[5];
		employee[0] = new SalariedEmployee(60000);
		employee[1] = new HourlyEmployee(40,20);
		employee[2] = new UnionizedHourlyEmployee(40,17,5000);
		employee[3] = new HourlyEmployee(50,15);
		employee[4] = new UnionizedHourlyEmployee(44,14,6000);
		
		System.out.println("total payroll: "+getTotalExpenses(employee));
	}
}

package Question2;

/**
 * An interface for objects of type Employee
 * @author tomas
 *
 */

public interface Employee {
	
	/**
	 * A method that employee shall have for ease of usage
	 */
	public int getYearlyPay();
}

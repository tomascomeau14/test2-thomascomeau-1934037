package Question2;

/**
 * An employee of type salary that implements the Employee interface
 * @author tomas
 *
 */
public class SalariedEmployee implements Employee {
	/**
	 * Represents their yearly pay
	 */
	private int yearlySalary;
	
	/**
	 * @return The yearly pay of employee
	 */
	public int getYearlyPay() {
		return yearlySalary;
	}
	
	/**
	 * Makes an employee of type salaryEmployee
	 * @param pay The yearly pay of an employee
	 */
	public SalariedEmployee(int pay) {
		yearlySalary = pay;
	}
}

package Question2;

/**
 * An employee of type union that implements the employee interface
 * @author tomas
 *
 */
public class UnionizedHourlyEmployee extends HourlyEmployee {
	/**
	 * Represents the amount of pension they recieve
	 */
	private int pensionContribution;
	/**
	 * Represents the amount of hours they work in a week
	 */
	private int hours;
	/**
	 * Represents the amount they make per hour
	 */
	private int wage;
	
	/**
	 * @return The amount that the employee get paid yearly
	 */
	public int getYearlyPay() {
		int yearPay = (hours * wage * 52) + pensionContribution;
		return yearPay;
	}
	
	/**
	 * Makes an employee of type pension
	 * @param hours
	 * @param wage
	 * @param pension
	 */
	public UnionizedHourlyEmployee(int hours, int wage, int pension) {
		super(hours,wage);
		pensionContribution = pension;
	}
}

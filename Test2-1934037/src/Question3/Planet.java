package Question3;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * @Override
	 * @return The boolean of wither two planets are the same or not
	 */
	public boolean equals(Object a) {
		Planet planet = (Planet)a;
		String name = this.getName();
		String name2 = planet.getName();
		if(name.equalsIgnoreCase(name2)) {
			int order1 = this.getOrder();
			int order2 = planet.getOrder();
			if(order1 == order2) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	/**
	 * Planets will have the same hash code if they have the same name and order.
	 * @Override
	 * @return a hashcode to easily identify if planets are the same
	 */
	public int hashCode() {
		return (int) name.hashCode() + order;
	}
	
	/**
	 * It returns a zero if they are the same
	 * @param plan
	 * @return int
	 * @Override
	 */
	public int compareTo(Planet plan) {
		String name = this.getName();
		String name2 = plan.getName();
		int nameComp = name.compareTo(name2);
		if(nameComp == 0) {
			name = this.getPlanetarySystemName();
			name2 = plan.getPlanetarySystemName();
			nameComp =-(name.compareTo(name2));
			return nameComp;
		}
		return nameComp;
	}
	
}


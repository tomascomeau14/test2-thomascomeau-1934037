package Question4;

//import java.util.ArrayList;

import java.util.Collection;
import java.util.Collections;

import Question3.Planet;

public class CollectionMethods {

	public static Collection<Planet> getInnerPlanets(Collection<Planet>planets){
		Collection<Planet> plan = Collections.emptyList();
		Planet[] temp = (Planet[]) planets.toArray();
		for(int i = 0;i < planets.size();i++) {
			int orderTemp = temp[i].getOrder();
			if(orderTemp <= 3 && orderTemp >= 1) {
				plan.add(temp[i]);
			}
		}
		return plan;
	}
}
